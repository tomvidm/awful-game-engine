#include <stdexcept>

#include "age/math.hpp"
#include "age/engine.hpp"

int main(int argc, char* argv[]) {
    age::Engine engine;
    engine.start();
    return 0;
}