#pragma once

#include <string>
#include <memory>

#include "SDL.h"

#include "age/math/vector.hpp"

namespace age {
    class Window {
    public:
        Window(const std::string& text, const vec2i size, const vec2i pos);
        ~Window();

    private:
        SDL_Window* sdl_window_ptr;
    };

    using SharedWindow = std::shared_ptr<Window>;
}