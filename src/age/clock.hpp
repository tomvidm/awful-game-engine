#pragma once

#include <chrono>

namespace age {
    using Time = std::chrono::system_clock::time_point;
    using Duration = std::chrono::system_clock::duration;
    using Milliseconds = std::chrono::milliseconds;
    using Seconds = std::chrono::seconds;
    using Minutes = std::chrono::minutes;
    using Hours = std::chrono::hours;

    using std::chrono::duration_cast;

    Time getCurrentTime();

    class Clock {
    public:
        Clock() : t0(std::chrono::system_clock::now()) {;}

        Duration reset();
        Duration getElapsedTime() const;
    private:
        Time t0;
    };
}