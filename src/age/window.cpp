#include "age/window.hpp"

namespace age {
    Window::Window(const std::string& text, const vec2i size, const vec2i pos) {
        sdl_window_ptr = SDL_CreateWindow(
            text.c_str(), pos.x, pos.y, size.x, size.y,SDL_WINDOW_OPENGL);
    }

    Window::~Window() {
        SDL_DestroyWindow(sdl_window_ptr);
    }
}