#pragma once

#include <SDL.h>

#include "age/clock.hpp"

namespace age {
    class InputEvent {
    public:
        InputEvent() = delete;
        InputEvent(SDL_Event& sdl_event);
    private:
        Clock poll_time;
    };
}