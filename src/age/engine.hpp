#pragma once

#include <queue>
#include <thread>
#include <future>

#include "age/window.hpp"
#include "age/input/event.hpp"

namespace age {
    class Engine {
    public:
        Engine();
        ~Engine();

        void start();
    private:
        void initWindow();
        void handleInput();

    private:
        bool running;

        SharedWindow window;
        std::queue<InputEvent> input_event_queue;
    };
}