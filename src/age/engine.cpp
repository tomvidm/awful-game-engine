#include <iostream>

#include "age/engine.hpp"

namespace age {
    Engine::Engine() : running(true) {
        if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
            throw std::runtime_error("Could not initialize the SDL2 library!");
        }
    }

    Engine::~Engine() {
        window.reset();
        SDL_Quit();
        std::cout << "SDL subsystems shut down." << std::endl;
    }

    void Engine::start() {
        initWindow();

        while (running) {
            handleInput();
        }
    }

    void Engine::initWindow() {
        window = std::make_shared<Window>(
            "Awful Game Engine (AGE)",
            vec2i(800, 600),
            vec2i(200, 200)
        );
    }

    void Engine::handleInput() {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
                break;
            }
        }
    }
}