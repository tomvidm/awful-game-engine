#pragma once

#include <memory>

#include "age/math.hpp"

namespace age {

    class Transformable;
    using SharedTransformable = std::shared_ptr<Transformable>;

    class Transformable {
    public:
        Transformable()
        : origin(0.0, 0.0), 
          position(0.0, 0.0), 
          scale(1.0, 1.0), 
          rotation(0.0),
          matrix_needs_update(true), 
          inv_matrix_needs_update(true),
          matrix(),
          inv_matrix(),
          parent_ptr(nullptr) {;}

        void move(vec2& disp);
        void rotate(AGE_FLOAT deg);

        void setOrigin(vec2& new_origin);
        void setPosition(vec2& new_position);
        void setScale(vec2& new_scale);
        void setRotation(AGE_FLOAT new_rotation);

        inline vec2 getOrigin() const { return origin; }
        inline vec2 getPosition() const { return position; }
        inline vec2 getScale() const { return scale; }
        inline AGE_FLOAT getRotation() const { return rotation; }

        void setParent(Transformable* parent);
        void setParent(SharedTransformable parent);

        mat3 getTransform() const;
        mat3 getInverseTransform() const;
    private:
        vec2 origin, position, scale;
        AGE_FLOAT rotation;

        mutable bool matrix_needs_update;
        mutable bool inv_matrix_needs_update;
        mutable mat3 matrix;
        mutable mat3 inv_matrix;

        Transformable* parent_ptr;
    };
}