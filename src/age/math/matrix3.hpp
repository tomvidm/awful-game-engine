#pragma once

#include <iostream>

#include "age/math/vector2.hpp"
#include "age/math/vector3.hpp"

namespace age {
    template <typename F>
    class Matrix3 {
    public:
        Matrix3()
        : a00(1.0), a01(0.0), a02(0.0),
          a10(0.0), a11(1.0), a12(0.0),
          a20(0.0), a21(0.0), a22(1.0) {;}

        Matrix3(
            F a00, F a01, F a02, 
            F a10, F a11, F a12,
            F a20, F a21, F a22) 
        : a00(a00), a01(a01), a02(a02),
          a10(a10), a11(a11), a12(a12),
          a20(a20), a21(a21), a22(a22) {;}

        F a00, a01, a02, 
          a10, a11, a12, 
          a20, a21, a22;

        static Matrix3<F> fromColumnVectors(const Vector2<F>& a, const Vector2<F>& b, const Vector2<F>& c);
        static Matrix3<F> fromColumnVectors(const Vector3<F>& a, const Vector3<F>& b, const Vector3<F>& c);
        static Matrix3<F> fromRowVectors(const Vector3<F>& a, const Vector3<F>& b, const Vector3<F>& c);
        static Matrix3<F> identity();

        Vector2<F> transform(const Vector2<F>& vec) const;
        Vector3<F> transform(const Vector3<F>& vec) const;
        Matrix3<F> inverse() const;
        Matrix3<F> transpose() const;
        F determinant() const;

        void print() const;
    };

    template <typename F>
    void Matrix3<F>::print() const {
        std::cout << a00 << " " << a01 << " " << a02 << std::endl
                  << a10 << " " << a11 << " " << a12 << std::endl
                  << a20 << " " << a21 << " " << a22 << std::endl;
    }

    template <typename F>
    bool operator == (const Matrix3<F>& a, const Matrix3<F>& b) {
        if ((a.a00 == b.a00) && (a.a01 == b.a01) && (a.a02 == b.a02), 
            (a.a10 == b.a10) && (a.a11 == b.a11) && (a.a12 == b.a12), 
            (a.a20 == b.a20) && (a.a21 == b.a21) && (a.a22 == b.a22)) {
            return true;
        } else {
            return false;
        }
    }

    template <typename F>
    Matrix3<F> operator * (const F factor, const Matrix3<F>& mat) {
        return Matrix3<F>(
            factor * mat.a00, factor * mat.a01, factor * mat.a02,
            factor * mat.a10, factor * mat.a11, factor * mat.a12,
            factor * mat.a20, factor * mat.a21, factor * mat.a22
        );
    }

    template <typename F>
    Matrix3<F> operator * (const Matrix3<F>& mat, const F factor) {
        return Matrix3<F>(
            factor * mat.a00, factor * mat.a01, factor * mat.a02,
            factor * mat.a10, factor * mat.a11, factor * mat.a12,
            factor * mat.a20, factor * mat.a21, factor * mat.a22
        );
    }

    template <typename F>
    Matrix3<F> Matrix3<F>::fromColumnVectors(const Vector2<F>& a, const Vector2<F>& b, const Vector2<F>& c) {
        return Matrix3<F>(
            a.x, b.x, c.x,
            a.y, b.y, c.y,
            0.0, 0.0, 1.0
        );
    }

    template <typename F>
    Matrix3<F> Matrix3<F>::fromColumnVectors(const Vector3<F>& a, const Vector3<F>& b, const Vector3<F>& c) {
        return Matrix3<F>(
            a.x, b.x, c.x,
            a.y, b.y, c.y,
            a.z, b.z, c.z
        );
    }

    template <typename F>
    Matrix3<F> Matrix3<F>::fromRowVectors(const Vector3<F>& a, const Vector3<F>& b, const Vector3<F>& c) {
        return Matrix3<F>(
            a.x, a.y, a.z,
            b.x, b.y, b.z,
            c.x, c.y, c.z
        );
    }

    template <typename F>
    Matrix3<F> Matrix3<F>::identity() {
        return Matrix3<F>(
            1.0, 0.0, 0.0,
            0.0, 1.0, 0.0,
            0.0, 0.0, 1.0
        );
    }

    template <typename F>
    Matrix3<F> matmul(const Matrix3<F>& a, const Matrix3<F>& b) {
        const Vector3<F> col_a = a.transform(Vector3<F>(b.a00, b.a10, b.a20));
        const Vector3<F> col_b = a.transform(Vector3<F>(b.a01, b.a11, b.a21));
        const Vector3<F> col_c = a.transform(Vector3<F>(b.a02, b.a12, b.a22));

        return Matrix3<F>::fromColumnVectors(col_a, col_b, col_c);
    }

    template <typename F>
    Vector2<F> Matrix3<F>::transform(const Vector2<F>& vec) const {
        return Vector2<F>(
            vec.x * a00 + vec.y * a01 + a02,
            vec.x * a10 + vec.y * a11 + a12
        );
    }

    template <typename F>
    Vector3<F> Matrix3<F>::transform(const Vector3<F>& vec) const {
        return Vector3<F>(
            vec.x * a00 + vec.y * a01 + vec.z * a02,
            vec.x * a10 + vec.y * a11 + vec.z * a12,
            vec.x * a20 + vec.y * a21 + vec.z * a22
        );
    }

    // Direct solution from https://en.wikipedia.org/wiki/Invertible_matrix
    template <typename F>
    Matrix3<F> Matrix3<F>::inverse() const {
        return Matrix3<F>(
            a11 * a22 - a12 * a21, a02 * a20 - a01 * a22, a01 * a12 - a02 * a11,
            a12 * a20 - a10 * a22, a00 * a22 - a02 * a20, a02 * a11 - a01 * a12,
            a10 * a11 - a01 * a20, a01 * a20 - a00 * a21, a00 * a11 - a01 * a10
        );
    }

    template <typename F>
    Matrix3<F> Matrix3<F>::transpose() const {
        return (F(1.0) / determinant()) * Matrix3<F>(
            a00, a10, a20,
            a01, a11, a21,
            a02, a12, a22
        );
    }

    template <typename F>
    F Matrix3<F>::determinant() const {
        return a00 * a11 * a22 + a01 * a12 * a20 + a02 * a10 * a21 - a02 * a11 * a20 - a01 * a10 * a22 - a00 * a12 * a21;
    }
}