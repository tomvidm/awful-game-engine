#pragma once

namespace age {
    template <typename T>
    T clamp(T min, T val, T max) {
        if (val < min) {
            return min;
        } else if (val > max) {
            return max;
        } else {
            return val;
        }
    }
}