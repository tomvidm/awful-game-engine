#pragma once

#include "age/math/config.hpp"

namespace age {
    const AGE_FLOAT pi = static_cast<AGE_FLOAT>(3.14159265359);
    const AGE_FLOAT deg2rad = pi / 180.0f;
}