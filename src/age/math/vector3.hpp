#pragma once

#include <cmath>

namespace age {
    template <typename F>
    class Vector3 {
    public:
        Vector3() : x(0.0), y(0.0), z(0.0) {;}
        Vector3(F x, F y, F z) : x(x), y(y), z(z) {;}
        Vector3(const Vector3<F>& other) : x(other.x), y(other.y), z(other.z) {;}

        Vector3<F> operator - () const;

        F x, y, z;
    };

    template <typename F>
    bool operator == (const Vector3<F>& a, const Vector3<F>& b) {
        if ((a.x == b.x) && (a.y == b.y) && (a.z == b.z)) {
            return true;
        } else {
            return false;
        }
    }

    template <typename F>
    Vector3<F> Vector3<F>::operator - () const {
        return Vector3<F>(-x, -y, -z);
    }

    template <typename F>
    Vector3<F> operator+(const Vector3<F>& u, const Vector3<F>& v) {
        return Vector3<F>(u.x + v.x, u.y + v.y, u.z + v.z);
    }

    template <typename F>
    Vector3<F> operator-(const Vector3<F>& u, const Vector3<F>& v) {
        return Vector3<F>(u.x - v.x, u.y - v.y, u.z - v.z);
    }

    template <typename F>
    Vector3<F> operator*(const Vector3<F>& u, const F factor) {
        return Vector3<F>(u.x * factor, u.y * factor, u.z * factor);
    }

    template <typename F>
    Vector3<F> operator*(const F factor, const Vector3<F>& u) {
        return Vector3<F>(u.x * factor, u.y * factor, u.z * factor);
    }

    template <typename F>
    F dot(const Vector3<F>& u, const Vector3<F>& v) {
        return u.x * v.x + u.y * v.y + u.z * v.z;
    }

    template <typename F>
    F magnitude_squared(const Vector3<F>& u) {
        return u.x * u.x + u.y * u.y + u.z * u.z;
    }

    template <typename F>
    F magnitude(const Vector3<F>& u) {
        return sqrt(magnitude_squared(u));
    }

    template <typename F>
    Vector3<F> projection(const Vector3<F>& u, const Vector3<F>& v) {
        return (dot(u, v) / dot(v, v)) * v;
    }

    template <typename F>
    Vector3<F> reflection(const Vector3<F>& u, const Vector3<F>& v) {
        return F(2.0) * projection(u, v) - u;
    }

    template <typename F>
    Vector3<F> normalized(const Vector3<F>& u) {
        return (F(1.0) / magnitude(u)) * u;
    }

    template <typename F>
    Vector3<F> cross(const Vector3<F>& u, const Vector3<F>& v) {
        return Vector3<F>(
            u.y * v.z - u.z * v.y,
            u.x * v.z - u.z * v.x,
            u.x * v.y - u.y * v.x
        );
    }
}
