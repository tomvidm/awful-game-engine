#pragma once

#include <cmath>

namespace age {
    template <typename F>
    class Vector2 {
    public:
        Vector2() : x(0.0), y(0.0) {;}
        Vector2(F x, F y) : x(x), y(y) {;}
        Vector2(const Vector2<F>& other) : x(other.x), y(other.y) {;}

        Vector2<F> operator - () const;

        F x, y;
    };

    template <typename F>
    bool operator == (const Vector2<F>& a, const Vector2<F>& b) {
        if ((a.x == b.x) && (a.y == b.y)) {
            return true;
        } else {
            return false;
        }
    }

    template <typename F>
    Vector2<F> Vector2<F>::operator - () const {
        return Vector2<F>(-x, -y);
    }

    template <typename F>
    Vector2<F> operator+(const Vector2<F>& u, const Vector2<F>& v) {
        return Vector2<F>(u.x + v.x, u.y + v.y);
    }

    template <typename F>
    Vector2<F> operator-(const Vector2<F>& u, const Vector2<F>& v) {
        return Vector2<F>(u.x - v.x, u.y - v.y);
    }

    template <typename F>
    Vector2<F> operator*(const Vector2<F>& u, const F factor) {
        return Vector2<F>(u.x * factor, u.y * factor);
    }

    template <typename F>
    Vector2<F> operator*(const F factor, const Vector2<F>& u) {
        return Vector2<F>(u.x * factor, u.y * factor);
    }

    template <typename F>
    F dot(const Vector2<F>& u, const Vector2<F>& v) {
        return u.x * v.x + u.y * v.y;
    }

    template <typename F>
    F magnitude_squared(const Vector2<F>& u) {
        return u.x * u.x + u.y * u.y;
    }

    template <typename F>
    F magnitude(const Vector2<F>& u) {
        return sqrt(magnitude_squared(u));
    }

    template <typename F>
    Vector2<F> projection(const Vector2<F>& u, const Vector2<F>& v) {
        return (dot(u, v) / dot(v, v)) * v;
    }

    template <typename F>
    Vector2<F> reflection(const Vector2<F>& u, const Vector2<F>& v) {
        return F(2.0) * projection(u, v) - u;
    }

    template <typename F>
    Vector2<F> normalized(const Vector2<F>& u) {
        return (F(1.0) / magnitude(u)) * u;
    }

    template <typename F>
    F cross(const Vector2<F>& u, const Vector2<F>& v) {
        return u.x * v.y - u.y * v.x;
    }
}
