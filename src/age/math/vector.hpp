#pragma once

#include "age/math/config.hpp"
#include "age/math/vector2.hpp"
#include "age/math/vector3.hpp"

namespace age {
    using vec2i = Vector2<int>;
    using vec2 = Vector2<AGE_FLOAT>;
    using vec3 = Vector3<AGE_FLOAT>;
}