#pragma once

#include "age/math/config.hpp"
#include "age/math/matrix3.hpp"

namespace age {
    using mat3 = Matrix3<AGE_FLOAT>;
}