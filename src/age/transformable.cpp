#include "age/transformable.hpp"
#include "age/math.hpp"

#include <cmath>

namespace age {
    void Transformable::move(vec2& disp) {
        position = position + disp;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }
    
    void Transformable::rotate(AGE_FLOAT deg) {
        rotation += deg;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    void Transformable::setOrigin(vec2& new_origin) {
        origin = new_origin;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    void Transformable::setPosition(vec2& new_position) {
        position = new_position;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    void Transformable::setScale(vec2& new_scale) {
        scale = new_scale;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    void Transformable::setRotation(AGE_FLOAT new_rotation) {
        rotation = new_rotation;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    void Transformable::setParent(Transformable* parent) {
        parent_ptr = parent;
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    void Transformable::setParent(SharedTransformable parent) {
        parent_ptr = parent.get();
        matrix_needs_update = true;
        inv_matrix_needs_update = true;
    }

    mat3 Transformable::getTransform() const {
        if (matrix_needs_update) {
            const AGE_FLOAT angle  = -rotation * deg2rad;
            const AGE_FLOAT cosine = static_cast<AGE_FLOAT>(std::cos(angle));
            const AGE_FLOAT sine   = static_cast<AGE_FLOAT>(std::sin(angle));
            const AGE_FLOAT sxc    = scale.x * cosine;
            const AGE_FLOAT syc    = scale.y * cosine;
            const AGE_FLOAT sxs    = scale.x * sine;
            const AGE_FLOAT sys    = scale.y * sine;
            const AGE_FLOAT tx     = -origin.x * sxc - origin.y * sys + position.x;
            const AGE_FLOAT ty     =  origin.x * sxs - origin.y * syc + position.y;

            matrix = mat3(sxc, sys, tx,
                         -sxs, syc, ty,
                          0.f, 0.f, 1.f);

            if (parent_ptr != nullptr) {
                const mat3 parent_matrix = parent_ptr->getTransform();
                matrix = matmul(parent_matrix, matrix);
            }

            matrix_needs_update = false;
        }

        return matrix;
    }

    mat3 Transformable::getInverseTransform() const {
        if (inv_matrix_needs_update) {
            inv_matrix = getTransform().inverse();
            inv_matrix_needs_update = false;
        }

        return inv_matrix;
    }
}