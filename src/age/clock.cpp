#include "age/clock.hpp"

namespace age {
    Time getCurrentTime() {
        return std::chrono::system_clock::now();
    }

    Duration Clock::reset() {
        Time current_time = getCurrentTime();
        Duration elapsed_time = current_time - t0;
        t0 = current_time;
        return elapsed_time;
    }

    Duration Clock::getElapsedTime() const {
        return getCurrentTime() - t0;
    }
}