#pragma once

#include "age/math/config.hpp"
#include "age/math/matrix.hpp"
#include "age/math/vector.hpp"
#include "age/math/constants.hpp"