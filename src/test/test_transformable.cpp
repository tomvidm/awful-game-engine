#include "gtest/gtest.h"

#include <iostream>

#include "age/math.hpp"
#include "age/transformable.hpp"

namespace age {
    TEST(TestTransformable, transformables_works_correctly) {
        Transformable t;
        
        t.setPosition(vec2(1.0, 1.0));
        t.setRotation(90.f);

        mat3 tmat = t.getTransform();

        EXPECT_EQ(tmat.transform(vec2(1.0, 0.0)), vec2(1.0, 2.0));
    }
}