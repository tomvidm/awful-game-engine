#include "gtest/gtest.h"

#include "age/math.hpp"

namespace age {
    TEST(TestVector, vec2_works_correctly) {
        vec2 a(1.0, 0.0);
        vec2 b(0.0, 1.0);

        EXPECT_EQ(a + b, vec2(1.0, 1.0));
        EXPECT_EQ(a - b, vec2(1.0, -1.0));
        EXPECT_EQ(2.f * a, vec2(2.0, 0.0));
        EXPECT_EQ(a * 2.f, vec2(2.0, 0.0));

        EXPECT_EQ(dot(a, b), 0.0);
        EXPECT_EQ(magnitude_squared(a + b), 2.0);
        
        EXPECT_EQ(projection(a + b, b), b);
        EXPECT_EQ(reflection(a, b), -a);
        EXPECT_EQ(cross(a, b), 1.0);
    }
}